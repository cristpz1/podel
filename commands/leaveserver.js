const config = require("../config.json");

module.exports.run = async (bot, message, args) => {

    if (!config.owner.includes(message.author.id)) return;

    let server = bot.guilds.cache.get(args[0]);
    
    let channelID;
    let channels = server.channels.cache;

    channelLoop:
    for (let key in channels) {
        let c = channels[key];
        if (c[1].type === "text") {
            channelID = c[0];
            break channelLoop;
        }
    }

    let channel = server.channels.cache.get(server.systemChannelID || channelID);
    channel.send(`**PSA:**\nPodel Bot will be leaving your server as it has been marked as spam, contact crist#0971 on discord for any futher information.`);

    server.leave().catch(err => {
       message.channel.send(`error: \n ${err.message}`);
    });

    message.channel.send(`left \`${server.name}\``);

}

module.exports.help = {
  name: "leaveserver",
  type: "owner"
}


