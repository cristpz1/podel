module.exports.run = async (bot, message, args) => {
    if (!args[0]) return message.channel.send(`you need to input a valid emoji name.`);
    const emoteRegex = /<:.+:(\d+)>/gm
    const animatedEmoteRegex = /<a:.+:(\d+)>/gm

    if (emoji = emoteRegex.exec(args[0])) {
        const url = "https://cdn.discordapp.com/emojis/" + emoji[1] + ".png?v=1"
        message.channel.send(url);
    }
    else if (emoji = animatedEmoteRegex.exec(args[0])) {
        const url = "https://cdn.discordapp.com/emojis/" + emoji[1] + ".gif?v=1"
        message.channel.send(url);
    }
    else {
        message.channel.send("I found nothing, sod off");
    }
};

module.exports.help = {
    name: "enlarge",
    aliases: ["e", "en"],
    type: "user"
};