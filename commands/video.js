const Discord = require("discord.js");
const superagent = require("superagent");

module.exports.run = async (bot, message, args) => {
    // DONT DO THIS!!! IM ONLY DOING IT AS A JOKE :3
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

    let { body } = await superagent.get("https://johnjohnspub.com/api/videos");
    let embed;

    if (args[0] == "-i" || args[0] == "--info") {
        let over = parseInt(args[1]);
        embed = new Discord.MessageEmbed()
            .setAuthor(body[over].author)
            .setTitle(`${body[over].id} - ${body[over].title} \nhttps://hoodvideo.miau.world/watch?v=${body[over].filename}`)
            .setThumbnail(body[over].thumbnail);
        message.channel.send(embed);
    } else if (args[0] == "-c" || args[0] == "--count") {
        message.channel.send(body.length-1);
    } else if (args[0] == "-s" || args[0] == "--search") {
        embed = "";
        query = args.join(" ").split(args[0])[1].trim();
        for (let i = 0; i < body.length; i++) {
            if (body[i].title.includes(query) || body[i].author.includes(query) || body[i].filename.includes(query)) embed += `${i} - ${body[i].title} - ${body[i].author} (${body[i].filename})\n`;
        }
        for (let i = 0; i < embed.length; i += 2000) {
            const toSend = embed.substring(i, Math.min(embed.length, i + 2000));
            message.channel.send(toSend);
        }
    } else if (args[0] == "-h" || args[0] == "--help") {
        message.channel.send("example usage:\np!video <video index> (get embed link for video)\np!video -l 10 (list the last 10 videos)\np!video -l 10-20 (list from the latest 10th video to the latest 20th video)\np!video -c (get the biggest video index, video count)\np!video -s <query> (search for video title, author or filename)\np!video -r (get random video embed link)\np!video -i <video index> (get info embed about video on that index)");
    } else if (args[0] == "-l" || args[0] == "--list") {
        let first = 0;
        let over = parseInt(args[1]);
        if (args[1].includes("-")) first = parseInt(args[1].split('-')[0])-1, over = parseInt(args[1].split('-')[1]);
        embed = "";
        for (let i = first; i < over; i++) {
            embed += `${i} - ${body[i].title} - ${body[i].author} (${body[i].filename})\n`;
        }
        for (let i = 0; i < embed.length; i += 2000) {
            const toSend = embed.substring(i, Math.min(embed.length, i + 2000));
            message.channel.send(toSend);
        }
    } else if (args[0] == "-r" || args[0] == "--random") {
        let random = Math.floor(Math.random() * body.length);
        message.channel.send(`${random}\nhttps://hoodvideo.miau.world/files/${body[random].filename}`);
    } else if (!args[0] || isNaN(args[0])) {
        embed = "";
        for (let i = 0; i < body.length; i++) {
            embed += `${i} - ${body[i].title} - ${body[i].author} (${body[i].filename})\n`;
        }
        for (let i = 0; i < embed.length; i += 2000) {
            const toSend = embed.substring(i, Math.min(embed.length, i + 2000));
            message.channel.send(toSend);
        }
    } else {
        let index = parseInt(args[0]);
        message.channel.send(`https://hoodvideo.miau.world/files/${body[index].filename}`);
    }
};

module.exports.help = {
    name: "video",
    type: "user"
}