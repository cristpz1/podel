const db = require("quick.db");

module.exports.run = async (bot, message, args) => {

    if (!args[0]) {
	await db.delete(`note_${message.author.id}`);
	message.channel.send("note removed");
	return;
    }
    if (args.join(" ").length < 257) { 

    await db.delete(`note_${message.author.id}`);
    await db.push(`note_${message.author.id}`, args.join(" "));

    message.channel.send(`note set to: \`${args.join(" ")}\``)

    } else return message.channel.send(`too many characters \`(${args.join(" ").length}/200)\``)

};

module.exports.help = {
    name: "note",
    aliases: ["setnote", "newnote"],
    type: "user"
}
