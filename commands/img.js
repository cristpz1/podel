const {
    GOOGLE_IMG_SCRAP,
    GOOGLE_IMG_INVERSE_ENGINE_URL,
    GOOGLE_IMG_INVERSE_ENGINE_UPLOAD,
    GOOGLE_QUERY,
} = require("google-img-scrap");

const Discord = require("discord.js");
const config = require("../config.json"),
    colour = config.colour;

module.exports.run = async (bot, message, args) => {

    const query = await GOOGLE_IMG_SCRAP({
        search: args.join(" "),
    });

    let i = 0;
    let newembed;

    let embedstart = new Discord.MessageEmbed()
        .setAuthor(`${message.author.tag} - Page ${i}/99`, `${message.author.avatarURL()}`)
        .setDescription(`[${query.result[i].title}](${query.result[i].originalUrl})`)
        .setImage(query.result[i].url)
        .setColor(colour)
        .setTimestamp()
        .setFooter('Podel, do ya thang', bot.user.avatarURL());

    message.channel.send(embedstart).then(async (startmsg) => {

        startmsg.react("◀️");
        startmsg.react("▶️");
        startmsg.react("⏹️");
        startmsg.react("🔢");

        const filter = (reaction, user) => {
            return (reaction.emoji.name === "◀️" || "▶️" || "⏹️") && user.id === message.author.id;
        };

        const collector = startmsg.createReactionCollector(filter, { time: 60000 });

        collector.on('collect', async (reaction, user) => {
            switch (reaction.emoji.name) {
                case "◀️":
                    if (i != 0) i--;
                    newembed = new Discord.MessageEmbed()
                        .setAuthor(`${user.tag} - Page ${i}/99`, `${user.avatarURL()}`)
                        .setDescription(`[${query.result[i].title}](${query.result[i].originalUrl})`)
                        .setImage(query.result[i].url)
                        .setColor(colour)
                        .setTimestamp()
                        .setFooter('Podel, do ya thang', bot.user.avatarURL());
                    startmsg.edit(newembed);
                    startmsg.reactions.removeAll();
                    startmsg.react("◀️");
                    startmsg.react("▶️");
                    startmsg.react("⏹️");
                    startmsg.react("🔢");
                    break;
                case "▶️":
                    i++;
                    newembed = new Discord.MessageEmbed()
                        .setAuthor(`${user.tag} - Page ${i}/99`, `${user.avatarURL()}`)
                        .setDescription(`[${query.result[i].title}](${query.result[i].originalUrl})`)
                        .setImage(query.result[i].url)
                        .setColor(colour)
                        .setTimestamp()
                        .setFooter('Podel, do ya thang', bot.user.avatarURL());
                    startmsg.edit(newembed);
                    startmsg.reactions.removeAll();
                    startmsg.react("◀️");
                    startmsg.react("▶️");
                    startmsg.react("⏹️");
                    startmsg.react("🔢");
                    break;
                case "🔢":
                    const filter = m => m.author.id == message.author.id && !isNaN(parseInt(m.content)) && parseInt(m.content) < 100 && parseInt(m.content) > -1;

                    message.channel.send("Pick a number from 0-99 (10 sec)").then((msg) => {
                        message.channel.awaitMessages(filter, { max: 1, time: 10000, errors: ['time'] })
                            .then(async collected => {
                                i = parseInt(collected.first().content);
                                newembed = new Discord.MessageEmbed()
                                    .setAuthor(`${user.tag} - Page ${i}/99`, `${user.avatarURL()}`)
                                    .setDescription(`[${query.result[i].title}](${query.result[i].originalUrl})`)
                                    .setImage(query.result[i].url)
                                    .setColor(colour)
                                    .setTimestamp()
                                    .setFooter('Podel, do ya thang', bot.user.avatarURL());

                                collected.first().delete();
                                msg.delete();
                                startmsg.edit(newembed);
                                startmsg.reactions.removeAll();
                                startmsg.react("◀️");
                                startmsg.react("▶️");
                                startmsg.react("⏹️");
                                startmsg.react("🔢");
                            });
                    });
                    break;
                case "⏹️":
                    return startmsg.delete();
            }
        });
    });
};

module.exports.help = {
    name: "img",
    aliases: ["image", "googleimages"],
    type: "user"
};


