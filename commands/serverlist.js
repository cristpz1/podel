const config = require("../config.json");

module.exports.run = async (bot, message, args) => {
  if (!config.owner.includes(message.author.id)) return;
  let list = bot.guilds.cache.map(g => g.name + " | Member Count: " + g.memberCount + " | Creation Date: " + g.createdAt + " | Joined At: " + g.joinedAt + " | Owner ID: " + g.owner.id + " | Guild ID: " + g.id + " | Guild Icon: " + `cdn.discordapp.com/icons/${g.id}/${g.icon}?size=1024`).join('\n\n\n');
  for (let i = 0; i < list.length; i += 2000) {
    const toSend = list.substring(i, Math.min(list.length, i + 2000));
    message.channel.send(toSend);
  }
  message.channel.send(`total number of servers: ${bot.guilds.cache.size}`);
}

module.exports.help = {
  name: "serverlist",
  type: "owner"
}
