const Canvas = require("canvas");

module.exports.run = async (bot, message, args) => {

let user = message.author.avatarURL({ format: 'png', dynamic: true, size: 1024 });
if (message.mentions.users.first() !== undefined) user = message.mentions.users.first().avatarURL({ format: 'png', dynamic: true, size: 1024 });
else if (args[0]) user = args.join('');
  const canvas = Canvas.createCanvas(353, 143);
  const ctx = canvas.getContext("2d");

  const background = await Canvas.loadImage(
    "https://cdn.glitch.com/5d94d2b3-55ae-4001-86e0-104c8c5e4005%2Ftheglobalelite.png?v=1588384940010"
  );
  const avatar = await Canvas.loadImage(user);
  await ctx.drawImage(avatar, 120, 15, 115, 115);
  await ctx.drawImage(background, 0, 0, canvas.width, canvas.height);


  message.channel.send(user.username + " is the worlds most global man", { files: [canvas.toBuffer()] });
};

module.exports.help = {
  name: "global",
  type: "user"
}
