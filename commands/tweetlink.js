module.exports.run = async(bot, message, args) => {
    let channel = message.channel;
    if (args[1] === "-s" || args[1] === "--silent") {
        channel = message.author;
        message.delete();
    }
    if (args[0].includes("twitter.com/")) {
        let query = args[0].split("?")[0];
	query = query.replace("twitter.com", "d.vxtwitter.com");
	query = query.concat(".mp4")

	channel.send(query);
    } else channel.send("not a twitter.com link"); 
};

module.exports.help = {
    name: "tweetlink",
    aliases: ["tlink", "tl", "tld", "downloadtweet"],
    type: "user"
};
