let superagent = require("superagent")
module.exports.run = async (bot, message, args) => {
    let rndVerseNum = Math.random() * (6236 - 1) + 1
    let { body } = await superagent.get('http://api.alquran.cloud/v1/ayah/' + rndVerseNum + '/en.asad');
    message.channel.send(body.data.text);
};

module.exports.help = {
    name: "quran",
    type: "user"
};
