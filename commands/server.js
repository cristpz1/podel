const Discord = require("discord.js");
let config = require('../config.json'),
  colour = config.colour;

module.exports.run = async (bot, message, args) => {

  let botsonly = message.guild.members.cache.filter(member => member.user.bot).size;
  let usersonly = message.guild.memberCount - botsonly;
  let guildowner = await message.guild.members.fetch(message.guild.ownerID);

  let sicon = `https://cdn.discordapp.com/icons/${message.guild.id}/${message.guild.icon}?size=128`;
  if (message.guild.features.includes("ANIMATED_ICON"))
    sicon = `https://cdn.discordapp.com/icons/${message.guild.id}/${message.guild.icon}.gif?size=128`;
  let serverembed = new Discord.MessageEmbed()
    .setTitle(`${message.author.tag} | Server Info`)
    .setColor(colour)
    .setThumbnail(sicon)
    .addField("Server Name", message.guild.name, true)
    .addField("Server ID", message.guild.id, true)
    .addField("Server Owner", guildowner, true)
    .addField("Server Owner ID", message.guild.owner.id, true)
    .addField('\u200b', '\u200b')
    .addField("Users", usersonly, true)
    .addField("Bots", botsonly, true)
    .addField('\u200b', '\u200b')
    .addField("Created On", message.guild.createdAt)
    .addField("Join Date", message.member.joinedAt)
    .setTimestamp()
    .setFooter("Podel, what continent am I in", bot.user.displayAvatarURL());

  message.channel.send(serverembed);
}

module.exports.help = {
  name: "server",
  aliases: ['serverinfo', 'sinfo'],
  type: "user"
}
