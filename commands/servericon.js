const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  message.channel.send(`https://cdn.discordapp.com/icons/${message.guild.id}/${message.guild.icon}?size=1024`);
}

module.exports.help = {
  name: "serverpic",
  aliases: ['servericon', 'sicon'],
  type: "user"
}
