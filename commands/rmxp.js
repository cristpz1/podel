const config = require("../config.json");
const db = require("quick.db");

module.exports.run = async (bot, message, args) => {

  if (message.guild.id !== config.guildID) return;

  if (message.member.hasPermission('MANAGE_MESSAGES')) {

    if (!args[0]) return message.reply("you need to provide a valid user ID");
    if (!args[1]) return message.reply("you need to provide a valid amount of XP to remove");
    if (isNaN(args[1])) return message.reply("you need to provide a valid amount of XP to remove");
    if (isNaN(args[0])) return message.reply("you need to provide a valid ID to remove XP from");

    db.subtract(`${args[0]}_xp`, parseInt(args[1]));

    message.channel.send("removed " + args[1] + " XP from ID " + args[0]);

  }
}

module.exports.help = {
  name: "rmxp",
  type: "mod"
}
