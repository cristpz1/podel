let superagent = require("superagent")
module.exports.run = async (bot, message, args) => {
    let randomChapNumB = Math.floor(Math.random() * (21 - 1) + 1); 
    let randomVerseB = Math.floor(Math.random() * (42 - 1) + 1);
    // the chapters for every name are different in length
    // (so are the number of verses) so the bot may sometimes
    // not return anything as the number doesn't match any in the api
    // oh well Ill figure out a way to set the range individually?

    // I know theirs a lot more chapters but IDC
    let names = [
        "John",
        "Roman",
        "Mark",
        "Luke",
        "Matt"
    ];
 
    let result = Math.floor(Math.random() * 5);

    // I have no idea if this will work
    let { body } = await superagent.get("https://bible-api.com/" + names[result] + "%20" + randomChapNumB + ":" + randomVerseB);
    message.channel.send(body.verses[0].text + " - " + names[result] + " " + randomChapNumB + ":" + randomVerseB);
};

module.exports.help = {
    name: "bible",
    type: "user"
};
