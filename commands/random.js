const Canvas = require("canvas");
const canvas = Canvas.createCanvas(1000, 1000);
const ctx = canvas.getContext("2d");

module.exports.run = async (bot, message, args) => {

let colours = [];

for (i in args) {
  colours.push(args[i])
}

if (colours.length > 0) {
for (var i = 0; i < 25; i++) {
for (var j = 0; j < 25; j++) {
number = Math.floor(Math.random() * colours.length);
ctx.fillStyle = `${colours[number]}`;
ctx.fillRect(j*40, i*40, 40, 40);
}
}

message.channel.send("", { files: [canvas.toBuffer()] });

} else {

for (var i = 0; i < 25; i++) {
for (var j = 0; j < 25; j++) {
randomColor = Math.floor(Math.random()*16777215).toString(16);
ctx.fillStyle = `#${randomColor}`;
ctx.fillRect(j*40, i*40, 40, 40);
}
}

message.channel.send("", { files: [canvas.toBuffer()] });

}

}

module.exports.help = {
   name: "random",
   type: "user"
}
