const Discord = require("discord.js");
const Canvas = require("canvas");
const db = require("quick.db");

module.exports.run = async (bot, message, args, tools) => {

if (args[0] != "large") {

const canvas = Canvas.createCanvas(1000, 1000);
const ctx = canvas.getContext("2d");

let user;
let avatar;

    let xp = db.all().filter((data) => data.ID.endsWith("xp")).sort((a, b) => b.data - a.data);
    xp.length = 19;
    let i = j = k = 0;
    let noxp = ["595630448676306944", "178535200756596745",
"245941955048112128"];
    for (i in xp) {
      if (noxp.includes(xp[i].ID.split('_')[0])) { 
xp.length++;
continue; 
};
      user = await bot.users.fetch(xp[i].ID.split('_')[0]);
      if (!user) return message.channel.send(user.id + " needs to be purged for this to work.")
      avatar = user.avatarURL({ format: 'png', dynamic: true, size: 256 });
      avatar = await Canvas.loadImage(avatar);
      await ctx.drawImage(avatar, k*250, j*250, 250, 250);  
       ctx.font = "16px Arial";
       ctx.fillStyle = "black";
       ctx.fillRect(k*250, (j+1)*250-16, 250, 16)
       ctx.fillStyle = "white";
       ctx.fillText(user.username, k*250, (j+1)*250-2);
      k++;
      if (k == 4) j++, k=0;
    }
message.channel.send("**The Hall of YAP - Top 16**", { files: [canvas.toBuffer()] });

} else {

const canvas = Canvas.createCanvas(2000, 1000);
const ctx = canvas.getContext("2d");

let user;
let avatar;

    let xp = db.all().filter((data) => data.ID.endsWith("xp")).sort((a, b) => b.data - a.data);
    xp.length = 35;
    let i = j = k = 0;
let noxp = ["595630448676306944", "178535200756596745",
"245941955048112128"];
    for (i in xp) {
      if (noxp.includes(xp[i].ID.split('_')[0])) { 
xp.length++; 
continue;
};
      user = await bot.users.fetch(xp[i].ID.split('_')[0]);
      if (!user) return message.channel.send(user.id + " needs to be purged for this to work.")
      avatar = user.avatarURL({ format: 'png', dynamic: true, size: 256 });
      avatar = await Canvas.loadImage(avatar);
      await ctx.drawImage(avatar, k*250, j*250, 250, 250);  
       ctx.font = "16px Arial";
       ctx.fillStyle = "black";
       ctx.fillRect(k*250, (j+1)*250-16, 250, 16)
       ctx.fillStyle = "white";
       ctx.fillText(user.username, k*250, (j+1)*250-2);
      k++;
      if (k == 8) j++, k=0;
    }
message.channel.send("**The Extended Hall of YAP - Top 32**", { files: [canvas.toBuffer()] });
}
}

module.exports.help = {
  name: "hallofyap",
  aliases: ["hall", "hly", "yap"],
  type: "user"
};

